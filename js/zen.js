var zen = (function(){

	var affCanvasId = "aff_canvas";

	function dlAffirmationsToList(url, list_id) {
		$.ajax({
			url: url,
			dataType: 'json',
			success: function(affirmations, status, jqXHR){
				$.each(affirmations, function(aff_index, aff){
					var aff_item = $('<div></div>')
						.text(aff)
						.addClass("affirmation")
						.addClass("center");
					$('#'+list_id).append(aff_item);
				});
			}
		});
	}

	function b64toBlob(b64Data, contentType, sliceSize) {
		// http://stackoverflow.com/a/16245768/1570062
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, {type: contentType});
		return blob;
	}

	function download(){
		loadAffirmations();
		window.scrollTo(0, 0);
		html2canvas($("#hidden_affirmation_list")).then(function(canvas) {
			var image = canvas.toDataURL("image/png");
			var blob = b64toBlob(image.split(',')[1], "image/png");
			var blobUrl = URL.createObjectURL(blob);
			window.open(blobUrl, '_blank');
			/*var link = document.createElement("a");
			link.download = "affirmation.png";
			link.href = blobUrl;
			link.click();*/
			//$("#hidden_affirmation_list").empty();
		});
	}

	function affirmationListClick(){
		var aff = $(this);
		aff.toggleClass("my-affirmation");
	}

	function loadAffirmations(){
		var hidden = $("#hidden_affirmation_list");
		hidden.empty();
		$(".my-affirmation").clone().appendTo(hidden);
		hidden.children(".my-affirmation").toggleClass("my-affirmation");
	}


	return {
		dlAffirmationsToList: dlAffirmationsToList,
		affirmationListClick: affirmationListClick,
		loadAffirmations: loadAffirmations,
		download: download
	}
}())

$("document").ready(function(){
	/*********************************
	/ This content is run on load
	/********************************/
	$("#download").click(zen.download);
});
